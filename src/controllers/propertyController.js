const Property = require('../models/property');

const getAllProperties = (req,res)=>{
    Property.find().sort({createdAt:-1})
    .then((result)=>{
        res.send({Properties :result})
    })
    .catch((err)=>{
        console.log(err);
    })
}
const getProperty = (req,res)=>{
    const {id} = req.params
    Property.findById(id)
    .then((result)=>{
        res.send({Property:result})
    })
    .catch((err)=>{
        console.log(err);
    })
}
const createProperty = (req,res)=>{
    console.log(req);
    const _Property = new Property(req.body)
    _Property.save()
    .then((result)=>{
        res.send({success: true,response:result})
    })
    .catch((err)=>{
        console.log(err);
    })
}
const updateProperty = (req,res)=>{
    const {id} = req.params
    Property.updateOne(id)
    .then((result)=>{
        res.send({success: true})
    })
    .catch((err)=>{
        console.log(err);
    })
}
const deleteProperty = (req,res)=>{
    const {id} = req.params
    Property.findByIdAndDelete(id)
    .then((result)=>{
        res.send({success: true})
    })
    .catch((err)=>{
        console.log(err);
    })
}

module.exports = {
    getAllProperties,
    getProperty,
    createProperty,
    updateProperty,
    deleteProperty
}