
const express = require('express');
const router = express.Router();
const propertyController = require('../controllers/propertyController')


router.get('/properties', propertyController.getAllProperties);
router.get('/property:id', propertyController.getProperty);
router.post('/property/create', propertyController.createProperty);
router.put('/property/update:id', propertyController.updateProperty);
router.post('/property/delete:id', propertyController.updateProperty);
  

module.exports = router