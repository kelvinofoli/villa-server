const mongoose  = require('mongoose')
const Schema = mongoose.Schema;

const propertySchema = new Schema({
    name : {type: String,required: true},
    description : {type: String,required: true},
    location : {type: String,required: true},
    bedrooms : {type: Number,required: true},
    bathrooms : {type: Number,required: true},
    isUse : {type: Boolean,required: true},
},{timestamps:true})

const Property = mongoose.model('Property',propertySchema)

module.exports = Property